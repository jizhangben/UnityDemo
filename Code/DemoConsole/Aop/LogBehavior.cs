﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity.InterceptionExtension;
using DemoConsole.Interface;

namespace DemoConsole.Aop
{
    public class LogBehavior : IInterceptionBehavior
    {
        public bool WillExecute
        {
            get
            {
                return true;
            }
        }

        public IEnumerable<Type> GetRequiredInterfaces()
        {
            return Type.EmptyTypes;
        }

        public IMethodReturn Invoke(IMethodInvocation input, GetNextInterceptionBehaviorDelegate getNext)
        {
            Console.WriteLine("LogBehavior Invoke befroe");

            Console.WriteLine("Arguments.Count:{0}", input.Arguments.Count);
          
            var result = getNext().Invoke(input, getNext);

            Console.WriteLine("LogBehavior Invoke end");
            Console.WriteLine("返回结果：{0}",result.ReturnValue);
            return result;
        }
    }
}
