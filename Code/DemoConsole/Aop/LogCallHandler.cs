﻿using System;

using Microsoft.Practices.Unity.InterceptionExtension;

namespace DemoConsole.Aop
{
    public class LogCallHandler : ICallHandler
    {
        public int Order
        {
            get; set;
        }

        public IMethodReturn Invoke(IMethodInvocation input, GetNextHandlerDelegate getNext)
        {
            Console.WriteLine("LogCallHandler Invoke befroe");

            Console.WriteLine("Arguments.Count:{0}", input.Arguments.Count);
            
            var result = getNext().Invoke(input, getNext);
            
            Console.WriteLine("LogCallHandler Invoke end");
            Console.WriteLine("返回结果：{0}", result.ReturnValue);
            return result;
        }
    }
}
