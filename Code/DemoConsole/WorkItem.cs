﻿

namespace DemoConsole
{
    public class WorkItem
    {
        public WorkType Type { get; set; }

        public string Title { get; set; }

        public decimal OverRate { get; set; }
    }

    public enum WorkType
    {
        /// <summary>
        /// 正常
        /// </summary>
        NomarlWork,

        /// <summary>
        /// 紧急
        /// </summary>
        UrgentWork,

        /// <summary>
        /// 长时间
        /// </summary>
        DaysWork

    }
}
