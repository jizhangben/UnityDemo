﻿
using DemoConsole.Interface;
using DemoConsole.Aop;

namespace DemoConsole.Impl
{
    public class DoNothing : IDoNothing
    {

        public WorkItem Work { get; private set; }

        [LogCallHandler]
        public bool Do(WorkItem work)
        {
            Work = work;
            Work.OverRate = 0;
            return false;
            
        }
        
        public string Display()
        {
            return "none";
        }
    }
}
