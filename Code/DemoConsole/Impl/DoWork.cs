﻿
using System;
using DemoConsole.Interface;

namespace DemoConsole.Impl
{
    public class DoWork : IDoWork
    {

        public WorkItem Work { get; private set; }

        
        public bool Do(WorkItem work)
        {
            this.Work = work;

            var isOver = false;
            switch (work.Type)
            {
                case WorkType.NomarlWork:
                    Console.WriteLine("任务：{0}，开始干活。", work.Title);
                    Work.OverRate = 1;
                    isOver = true;
                    break;

                case WorkType.UrgentWork:
                    Console.WriteLine("任务：{0}，赶紧干活。", work.Title);
                    Work.OverRate = 0;
                    throw new Exception("干屁，老子不干了");

                case WorkType.DaysWork:
                    Console.WriteLine("任务：{0}，开始干活。", work.Title);
                    Work.OverRate = 0.3m;
                    break;

            }
            return isOver;
        }
    }
}
