﻿using System;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.InterceptionExtension;
using DemoConsole.Impl;
using DemoConsole.Interface;
using DemoConsole.Aop;

namespace DemoConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = new UnityContainer();

            container.AddNewExtension<Interception>();
            
            container.RegisterType<IDoWork, DoWork>(
                new Interceptor<InterfaceInterceptor>(),
                new InterceptionBehavior<LogBehavior>(),
                new AdditionalInterface<IDoWork>());
            /*
            container.RegisterType<IDoNothing, DoNothing>(
            new Interceptor<InterfaceInterceptor>(),
            new InterceptionBehavior<LogBehavior>(),
            new AdditionalInterface<IDoNothing>());
            */

            container.RegisterType<IDoNothing, DoNothing>().Configure<Interception>()
                .SetInterceptorFor<IDoNothing>(new InterfaceInterceptor()); 
           /*
           container.RegisterType<IDoNothing, DoNothing>(
               new Interceptor<InterfaceInterceptor>(),
               new InterceptionBehavior<LogBehavior>());
           */
           var woker = container.Resolve<IDoWork>();

            Console.WriteLine(woker.Do(new WorkItem { Type = WorkType.NomarlWork, Title = "对接支付" }));
            Console.WriteLine("--------------------------------------------------------\n");

            Console.WriteLine(woker.Do(new WorkItem { Type = WorkType.DaysWork, Title = "开发新版系统" }));
            Console.WriteLine("--------------------------------------------------------\n");

            var nowoker = container.Resolve<IDoNothing>();
            Console.WriteLine(nowoker.Do(new WorkItem { Type = WorkType.NomarlWork, Title = "改Bug" }));
            Console.WriteLine("--------------------------------------------------------\n");
            
            Console.WriteLine("Display:{0}", nowoker.Display());
            Console.WriteLine("--------------------------------------------------------\n");
                       

            Console.ReadLine();

        }
    }
}
