﻿

using DemoConsole.Aop;

namespace DemoConsole.Interface
{
    public interface IDoWork
    {
        
        WorkItem Work { get;  }


        bool Do(WorkItem work);
    }
}
