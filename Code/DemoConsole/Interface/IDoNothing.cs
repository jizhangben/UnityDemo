﻿

namespace DemoConsole.Interface
{
    public interface IDoNothing
    {
        WorkItem Work { get; }

        string Display();

        bool Do(WorkItem work);
    }
}
